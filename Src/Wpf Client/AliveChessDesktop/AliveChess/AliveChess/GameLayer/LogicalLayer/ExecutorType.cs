﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AliveChess.GameLayer.LogicLayer
{
    public enum ExecutorType
    {
        AuthorizeResponse    = 1,
        GetMapResponse       = 22,
        GetGameStateResponse = 32,
    }
}
