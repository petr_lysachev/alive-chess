﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace WindowsMobileClientAliveChess.GameLayer.Controls
{
    public partial class SimpleDisputeControl : UserControl
    {
        public SimpleDisputeControl(Game context)
        {
            InitializeComponent();
        }

        public void Initialize()
        {

        }

        public void InitializeTrade() { }
        public void InitializePayOff() { }
        public void InitializeJoinEmperies() { }
        public void InitializeEmpire() { }
        public void InitializeCapitulate() { }
        public void InitializeCaptureCastle() { }

    }
}
