﻿namespace WindowsMobileClientAliveChess.GameLayer.AliveChessGraphics
{
    public interface IGraphicManager
    {
        void Draw();
    }
}
