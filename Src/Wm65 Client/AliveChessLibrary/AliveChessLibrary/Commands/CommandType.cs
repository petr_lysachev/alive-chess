﻿namespace AliveChessLibrary.Commands
{
    public enum CommandType
    {
        Undefinite       = 0,
        BigMapCommand    = 1,
        DialogCommand    = 2,
        BattleCommand    = 3,
        CastleCommand    = 4,
        TradingCommand   = 5,
        RegisterCommand  = 6,
        EmpireCommand    = 7,
        StatisticCommand = 8,
        ErrorCommand     = 9
    }
}
