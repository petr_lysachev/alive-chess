﻿namespace AliveChessLibrary.Interaction
{
    public enum StimulusType
    {
        None                = 0,
        ProposeBattle       = 1,
        ProposeTrading      = 2,
        ProposeCooperation  = 3,
        ProposePayOff       = 4,
        ProposeCapitulation = 5,
        ProposeCondition    = 6,
        Compromise          = 7,
        CoerceToBattle      = 8
    }
}
