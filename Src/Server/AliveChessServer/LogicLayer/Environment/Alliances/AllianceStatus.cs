﻿namespace AliveChessServer.LogicLayer.Environment.Alliances
{
    public enum AllianceStatus
    {
        Union  = 0,
        Empire = 1,
    }
}
