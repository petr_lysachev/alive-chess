// Generated by the protocol buffer compiler.  DO NOT EDIT!

package AliveChessLibrary.Commands.RegisterCommands {

  import com.google.protobuf.*;
  import com.hurlant.math.BigInteger;
  
  import flash.utils.*;
  public final class RegisterResponse extends Message {
    public function RegisterResponse() {
      registerField("isSuccessed","",Descriptor.BOOL,Descriptor.LABEL_OPTIONAL,1);
    }
    // optional bool isSuccessed = 1;
    public var isSuccessed:Boolean = false;   
  
  }
}