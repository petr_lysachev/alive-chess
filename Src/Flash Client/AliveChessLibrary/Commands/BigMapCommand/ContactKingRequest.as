// Generated by the protocol buffer compiler.  DO NOT EDIT!

package AliveChessLibrary.Commands.BigMapCommand {

  import com.google.protobuf.*;
  import flash.utils.*;
  import com.hurlant.math.BigInteger;
  public final class ContactKingRequest extends Message {
    public function ContactKingRequest() {
      registerField("opponentId","",Descriptor.INT32,Descriptor.LABEL_OPTIONAL,1);
    }
    // optional int32 opponentId = 1;
    public var opponentId:int = 0;
    
  
  }
}