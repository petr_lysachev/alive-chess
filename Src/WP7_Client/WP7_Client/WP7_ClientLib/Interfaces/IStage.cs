﻿namespace AliveChessLibrary.Interfaces
{
    /// <summary>
    /// уровень
    /// </summary>
    public interface IStage
    {
        int Id { get; set; }
    }
}
