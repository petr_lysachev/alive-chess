﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AliveChessLibrary.Mathematic.GeometryUtils
{
    public class PointF
    {
        private float x;
        private float y;

        public PointF(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
